const router = require('express').Router();
const path = require('path');
const api_path = '../../api';

// define the home page route
router.use('/comic-vine', require(path.join(api_path, 'comic-vine', 'comic-vine.router')));
router.get('/', (req, res) => {
    res.json({
        ok: true,
        router: 'api'
    });
});

module.exports = router;