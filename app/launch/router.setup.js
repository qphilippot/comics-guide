module.exports = function(app) {
    // setup body-request parsing
    const bodyParser = require('body-parser');
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    const settings = require('./router.config');

    // bind default router
    try {
        settings.defaultRouters.forEach(path => {
            const parts = path.split('/');
            const name = parts[parts.length - 1];
            const prefix = '/' + name.split('.')[0];
            app.use(prefix, require(path))
        });
    }

    catch(error) {
        console.error(error);
    }  
};