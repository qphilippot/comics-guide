module.exports = function(app) {
    require('./router.setup')(app);
    require('./server.setup')(app);
};