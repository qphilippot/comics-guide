
module.exports = function(app) {
    const settings = require('./server.config');
    app.listen(settings.defaultPort, function () {
        console.log(`comic-guide is listening on port ${settings.defaultPort} !`)
    });
};