class Pool {
    constructor(factory) {
        this.factory = factory;
        this.container = [];
    }

    create(settings) {
        const instance = new this.factory(settings);
        instance.avaible = false;
        instance.recycle = () => {
            instance.clear();
            instance.avaible = true;
            this.container.push(instance);
        }

        return instance;
    }

    getInstance(settings) {
        if (this.container.length === 0) {
            return this.create(settings);
        }

        else {
            const instance = container.pop();
            return instance;
        }
    }
}

module.exports = Pool;