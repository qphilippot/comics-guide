const querystring = require('querystring');
const https = require('https');



class RestAPI {
    constructor(settings, credentials) {
        this.api_key = credentials.API_KEY,
        this.responseformat = settings.format
    }
    
    onQueryEnd() {}

    query(options) {
        return new Promise((resolve, reject) => {
            https.get(options, (resp) => {
                let data = '';
              
                // A chunk of data has been recieved.
                resp.on('data', (chunk) => {
                  data += chunk;
                });
              
                // The whole response has been received. Print out the result.
                resp.on('end', () => {
                    const response = this.onQueryEnd(data, options);
                    resolve(response);
                });
              
              }).on("error", (err) => {
                reject(err);
            });
        });
    }
}

module.exports = RestAPI;