module.exports = {
    endpoint: 'comicvine.gamespot.com',
    format: 'json',
    ressources: {
        character: require('./ressources/character.config'),
        concept: require('./ressources/concept.config'),
        origin: require('./ressources/origin.config'),
        object: require('./ressources/object.config'),
        location: require('./ressources/location.config'),
        issue: require('./ressources/issue.config'),
        story_arc: require('./ressources/story_arc.config'),
        volume: require('./ressources/volume.config'),
        publisher: require('./ressources/publisher.config'),
        person: require('./ressources/person.config'),
        team: require('./ressources/team.config'),
        video: require('./ressources/video.config')
    },

    searchable: {
        character: true,
        concept: true,
        origin: true,
        object: true,
        location: true,
        issue: true,
        story_arc: true,
        volume: true,
        publisher: true,
        person: true,
        team: true,
        video: true
    }
};