const router = require('express').Router();
const ComicVine = require('./comic-vine.model');

// define the home page route
router.get('/search/character', 
    async (req, res) => {
        const result = await ComicVine.search(req.query.query, 'character');
        res.json(result);
    }
);

router.get('/search/character/:query', 
    async (req, res) => {
        const result = await ComicVine.search(req.params.query, 'character');
        res.json(result);
    }
);

router.get('/search/:query', 
    async (req, res) => {
        const result = await ComicVine.search(req.params.query);
        res.json(result);
    }
);

router.get('/search', 
    async (req, res) => {
        const result = await ComicVine.search(req.query.query);
        res.json(result);
    }
);

router.get('/', (req, res) => {
    res.json({
        ok: true,
        api: 'comic-vine'
    });
});

module.exports = router;