const settings = require('./comic-vine.config');
const credentials = require('./comic-vine.credentials');
const RestAPI = require('./rest.model');
const querystring = require('querystring');
const Pool = require('./pool.model');
const ComicVineRequest = require('./comic-vine.request');

// todo create pool of request object

class ComicVine extends RestAPI {
    constructor() {
        super(settings, credentials);
        this.request = new Pool(ComicVineRequest);
        this.settingsString = querystring.stringify(this.settings);
    }

    async searchCharacter(query) {
        return await this.search(query, 'character');
    }

    async search(query, ressources) {
        const request = this.request.getInstance();
        request.setRessources('search', ressources);
        request.setQuery(query);
        const options = request.generate();
        const response = await this.query(options);
        request.recycle();
        return response;
    }

    onQueryEnd(data, options) {
        let response = JSON.parse(data);
        response.credit = settings.endpoint;
        response.request = options.path;
        return response;
    }
    
    async searchConcept(query) {
       return await this.search(query, 'concept');
    }

    async searchOrigin(query) {
        return await this.search(query, 'origin');
    }

    async searchObject(query) {
        return await this.search(query, 'object');
    }

    async searchLocation(query) {
        return await this.search(query, 'location');
    }

    async searchIssue(query) {
        return await this.search(query, 'issue');
    }

    async searchStoryArc(query) {
        return await this.search(query, 'story_arc');
    }

    async searchVolume(query) {
        return await this.search(query, 'volume');
    }

    async searchPublisher(query) {
        return await this.search(query, 'publisher');
    }

    async searchPerson(query) {
        return await this.search(query, 'person');
    }

    async searchTeam(query) {
        return await this.search(query, 'team');
    }

    async searchVideo(query) {
        return await this.search(query, 'video');
    }
}


module.exports = new ComicVine();