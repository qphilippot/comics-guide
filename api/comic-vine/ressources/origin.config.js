module.exports = {
    format: 'json',
    fields: {
        api_detail_url: true,
        character_set: true,	
        id: true,
        name: true,
        profiles: true,	
        site_detail_url: true
    }
};