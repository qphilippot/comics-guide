module.exports = {
    format: 'json',
    fields:	{
        aliases: true,
        api_detail_url: false,
        characters: false,
        date_added: false,
        date_last_updated: false,
        deck: true,
        description: false,
        id: true,
        image: false,
        location_adress: false,
        location_city: false,
        location_state: false,
        name: true,
        site_detail_url: false,
        story_arc: false,
        teams: true,
        volumes: true
    }    
};