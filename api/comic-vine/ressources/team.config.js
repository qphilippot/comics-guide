module.exports = {
    format: 'json',
    fields:	{
        aliases: true,
        api_detail_url: false,
        character_enemies: false,
        character_friends: false,
        characters: true,
        count_of_issue_appearances: true,
        count_of_team_members: true,
        date_added: false,
        date_last_updated: false,
        deck: true,
        description: false,
        disbanded_in_issues: false,
        first_appeared_in_issue: true,
        id: true,
        image: false,
        issue_credits: true,
        issues_disbanded_in: true,
        movies: false,
        name: true,
        publisher: true,
        site_detail_url: false,
        story_arc_credits: false,
        volume_credits: true
    }    
};