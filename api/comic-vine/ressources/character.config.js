module.exports = {
    format: 'json',
    fields:	{
        aliases: true,
        api_detail_url: false,
        birth: true,
        character_enemies: false,
        character_friends: false,
        count_of_issue_appearances: true,
        creators: true,
        date_added: false,
        date_last_updated: false,
        deck: true,
        description: false,
        first_appeared_in_issue: true,
        gender: true,
        id: true,
        image: false,
        issue_credits: true,
        issues_died_in: true,
        movies: false,
        name: true,
        origin: true,
        powers: false,
        publisher: true,
        real_name: true,
        site_detail_url: false,
        story_arc_credits: false,
        team_enemies: false,
        team_friends: false,
        volume_credits: true
    }    
};