module.exports = {
    format: 'json',
    fields:	{
        api_detail_url: false,
        deck: true,
        hd_url: true,
        high_url: true,
        id: true,
        image: false,
        length_seconds: true,
        low_url: true,
        movies: false,
        name: true,
        publis_date: true,
        site_detail_url: false,
        url: false,
        user: false
    }    
};