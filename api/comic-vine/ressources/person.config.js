module.exports = {
    format: 'json',
    fields:	{
        aliases: true,
        api_detail_url: false,
        birth: true,
        count_of_issue_appearances: false,
        country: true,
        created_characters: true,
        date_added: false,
        date_last_updated: false,
        death: false,
        deck: true,
        description: false,
        email: false,
        gender: false,
        hometown: false,
        id: true,
        image: false,
        issue_credits: false,
        name: true,
        site_detail_url: false,
        story_arc_credits: false,
        volume_credits: true,
        website: true
    }    
};