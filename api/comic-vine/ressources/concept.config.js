module.exports = {
    format: 'json',
    fields: {
        aliases: true,
        api_detail_url: true,
        count_of_issue_appearances: true,
        date_added: true,
        date_last_updated: true,
        deck: true,
        description: true,
        first_appeared_in_issue: true,
        id: true,
        image: true,
        name: true,
        site_detail_url: true,
        start_year: true
    }
};