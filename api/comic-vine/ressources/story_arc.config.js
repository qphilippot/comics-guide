module.exports = {
    format: 'json',
    fields:	{
        aliases: true,
        api_detail_url: false,
        count_of_issue_appearances: true,
        date_added: false,
        date_last_updated: false,
        deck: true,
        description: false,
        first_appeared_in_issue: true,
        id: true,
        image: false,
        issues: true,
        movies: false,
        name: true,
        publisher: true,
        site_detail_url: false
    }    
};