module.exports = {
    format: 'json',
    fields:	{
        aliases: true,
        api_detail_url: false,
        count_of_issue_appearances: true,
        date_added: false,
        date_last_updated: false,
        deck: true,
        description: false,
        first_appeared_in_issue: true,
        id: true,
        image: false,
        issue_credits: true,
        movies: false,
        name: true,
        site_detail_url: false,
        start_year: false,
        story_arc_credits: false,
        volume_credits: true
    }    
};