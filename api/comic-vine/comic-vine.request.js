const settings = require('./comic-vine.config');
const credentials = require('./comic-vine.credentials');
const RestAPI = require('./rest.model');
const querystring = require('querystring');

// todo create pool of request object

class ComicVineRequest {
    constructor() {
        this.options = {
            hostname: settings.endpoint,
            headers: { 'User-Agent': 'Mozilla/5.0' }
        };

        this.clear();
        this.credentials = querystring.stringify({ api_key: credentials.API_KEY });
        this.format = settings.format;
    }

    clear() {
        this.options.path = '';
        this.ressource = null;
        this.fields = null; 
        this.query = null;
        this.format = settings.format;
        this.ressourceSettings = null;
    }

    getDefaultFields(ressouceName) {
        const fields = settings.ressources[ressouceName].fields;
        return Object.keys(fields).reduce((acc, value) => {
            if (fields[value] === true) {
                // add ',' only if acc is not empty
                return acc + (acc !== '' ? ',' + value : value);
            }

            else {
                return acc;
            }
        }, '');
    }

    setRessources(ressource, params) {
        this.ressource = ressource;
        
        if (ressource === 'search' && typeof params !== 'undefined') {
            const tokens = params.split(',');
            let ressourcesAllowed = tokens.filter(token => settings.searchable[token] === true);

            if (ressourcesAllowed.length !== tokens.length) {
                throw new Error('some invalid params are set in :' + params);
            }

            if (tokens.length === 1) {
                this.setFieldList(this.getDefaultFields(params));
            }

            this.ressourceSettings = ressourcesAllowed.join(',');
        }
    }

    setFieldList(fields) {
        this.fields = fields;
    }

    getDefaultOptions(ressource) {
        this.options.path = '/api/' + ressource + '/?' + this.settingsString; // + '&sort=name:asc&filter=name:Walking%20Dead'
        return this.options;
    }

    generate() {
        this.options.path = '/api/' + this.ressource + '/?' + this.credentials;

        if (this.fields !== null) {
            this.options.path += '&field_list=' + this.fields;
        }

        if (this.query !== null) {
            this.options.path += '&query=' + this.query;
        }

        if (this.ressource === 'search' && this.ressourceSettings !== null) {
            this.options.path += '&resources=' + this.ressourceSettings;
        }

        this.options.path += '&format=' + this.format;
        

        return this.options;
    }

    setQuery(query) {
        if (this.ressource === 'search') {
            this.query = encodeURI(query);
        }

        else {
            throw new Error('query field is only avaible with search ressource');
        }
    }
}

ComicVineRequest.use = function(settings) {

};

module.exports = ComicVineRequest;