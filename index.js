const ComicVine = require('./api/comic-vine/comic-vine.model');
const express = require('express');
const app = express();

const launch = require('./app/launch');
launch(app);

// (async () => {
//     // console.log(await ComicVine.searchCharacter('hal jordan'));
//     // console.log(await ComicVine.searchConcept('mutant'));
//     // console.log(await ComicVine.searchOrigin('Mutant'));
//     // console.log(await ComicVine.searchObject('batarang'));
//     console.log(await ComicVine.search('Batarang'));
//     //console.log(ComicVine.getDefaultCharacterFields());
// })();